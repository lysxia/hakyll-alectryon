# 0.1.2.0

- Adapt to Alectryon 1.4.0

# 0.1.1.0

- Fix Pygments cache

# 0.1.0.0

* Create hakyll-alectryon
