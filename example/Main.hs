{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Functor ((<&>))
import qualified Hakyll
import qualified Hakyll.Alectryon as Alectryon

main :: IO ()
main = Alectryon.hakyll $ \opt -> do
  Hakyll.match "posts/*.md" $ do
    Hakyll.route (Hakyll.setExtension ".html")
    Hakyll.compile $ do
      Hakyll.getResourceBody
        >>= Hakyll.readPandoc
        >>= Alectryon.tryTransform opt
        <&> Hakyll.writePandoc
