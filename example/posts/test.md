---
title: Test post, please ignore
alectryon: []
alectryon-cache: "posts/test/"
---

# Exhibit A

```coq
Definition test := "This is a test".
```

# Exhibit B

```alectryon
Definition test := 0.
Check test.
```

# Exhibit C

```alectryon
Theorem test_theorem : test = 0.
Proof.
  cbv.
  reflexivity.
Qed.
```
